<?php

namespace App\Http\Controllers;

use App\Charts\CovidChart;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class InfoController extends Controller
{
  public function index()
  {
    $global = Http::get('https://api.covid19api.com/summary');
    $allcountry = Http::get('https://api.kawalcorona.com/');
    $allprov = Http::get('https://data.covid19.go.id/public/api/prov.json');
    $indonesia = Http::get('https://data.covid19.go.id/public/api/update.json');


    $indonesia = $indonesia->json();
    $allprov = $allprov->json();
    $allcountry = $allcountry->json();
    $global = $global->json();

    usort($global['Countries'], function ($a, $b) { //Sort the array using a user defined function
      return $a['TotalConfirmed'] > $b['TotalConfirmed'] ? -1 : 1; //Compare the scores
    });

    $harian = collect($indonesia['update']['harian']);
    //details data
    $labels = $harian->pluck('key_as_string');
    $labels = $labels->map(function ($month) {
      return Carbon::parse($month)->format('d M');
    });
    $jumlah_positif_kum = $harian->pluck('jumlah_positif_kum.value');
    $jumlah_sembuh_kum = $harian->pluck('jumlah_sembuh_kum.value');
    $jumlah_meninggal_kum = $harian->pluck('jumlah_meninggal_kum.value');
    $jumlah_dirawat_kum = $harian->pluck('jumlah_dirawat_kum.value');

    //generate chart
    $chart = new CovidChart;
    $chart->labels($labels);
    $chart->dataset('Jumlah Positif', 'line', $jumlah_positif_kum)->fill(false)->color('#ECC94B')->backgroundColor('#ECC94B');
    $chart->dataset('Jumlah Sembuh', 'line', $jumlah_sembuh_kum)->fill(false)->color('#4299E1')->backgroundColor('#4299E1');
    $chart->dataset('Jumlah Meninggal', 'line', $jumlah_meninggal_kum)->fill(false)->color('#000000')->backgroundColor('#000000');
    $chart->dataset('Jumlah Dirawat', 'line', $jumlah_dirawat_kum)->fill(false)->color('#00008B')->backgroundColor('#00008B');

    return view('index', compact('global', 'allcountry', 'indonesia', 'allprov', 'chart'));
  }
}
