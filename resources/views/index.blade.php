@extends('layouts.app')

@php
    \Carbon\Carbon::setLocale('id');
@endphp
@section('content')
<div class="jumbotron">
	<div class="container">
    <br><h1 class="display-3 text-center">INFO CORONA</h1>
		<p class="lead m-0 text-center">Coronavirus Global & Indonesia Live Data</p>
	</div>
</div>

<div class="row">
  <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
    <div class="card bg-danger img-card box-primary-shadow">
      <div class="card-body">
        <div class="d-flex">
          <div class="text-white">
            <p class="text-white mb-0">TOTAL POSITIF</p>
            <h2 class="mb-0 number-font">{{ number_format($global['Global']['TotalConfirmed'],0,",",".") }}</h2>
            <small>+ {{ number_format($global['Global']['NewConfirmed'],0,",",".") }}</small>
            <p class="text-white mb-0">ORANG</p>
          </div>
          <div class="ml-auto"> <img src="../uploads/sad-u6e.png" width="50" height="50" alt="Positif"> </div>
        </div>
      </div>
    </div>
  </div><!-- COL END -->
  <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
    <div class="card bg-success img-card box-secondary-shadow">
      <div class="card-body">
        <div class="d-flex">
          <div class="text-white">
            <p class="text-white mb-0">TOTAL SEMBUH</p>
            <h2 class="mb-0 number-font">{{ number_format($global['Global']['TotalRecovered'],0,",",".") }}</h2>
            <small>+ {{ number_format($global['Global']['NewRecovered'],0,",",".") }}</small>
            <p class="text-white mb-0">ORANG</p>
          </div>
          <div class="ml-auto"> <img src="../uploads/happy-ipM.png" width="50" height="50" alt="Positif"> </div>
        </div>
      </div>
    </div>
  </div><!-- COL END -->
  <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
    <div class="card bg-dark img-card box-success-shadow">
      <div class="card-body">
        <div class="d-flex">
          <div class="text-white">
            <p class="text-white mb-0">TOTAL MENINGGAL</p>
            <h2 class="mb-0 number-font">{{ number_format($global['Global']['TotalDeaths'],0,",",".") }}</h2>
            <small>+ {{ number_format($global['Global']['NewDeaths'],0,",",".") }}</small>
            <p class="text-white mb-0">ORANG</p>
          </div>
          <div class="ml-auto"> <img src="../uploads/emoji-LWx.png" width="50" height="50" alt="Positif"> </div>
        </div>
      </div>
    </div>
  </div><!-- COL END -->
  <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
    <div class="card  bg-orange img-card box-success-shadow">
      <div class="card-body">
        <div class="d-flex">
          <div class="text-white">
            <p class="text-white mb-0">TOTAL DIRAWAT</p>
            <h2 class="mb-0 number-font">{{ number_format($global['Global']['TotalConfirmed']-$global['Global']['TotalRecovered']-$global['Global']['TotalDeaths'],0,",",".") }}</h2>
            <small>+ {{ number_format($global['Global']['NewConfirmed']-$global['Global']['NewRecovered']-$global['Global']['NewDeaths'],0,",",".") }}</small>
            <p class="text-white mb-0">ORANG</p>
          </div>

          <div class="ml-auto"> <img src="../uploads/dirawat.png" width="50" height="50" alt="Positif"> </div>
        </div>
      </div>
    </div>
  </div><!-- COL END -->
  <div class="col text-center"><p>Update terakhir : {{ \Carbon\Carbon::now()->format('d F Y H:i') }} WIB</p></div>
</div>
<!-- ROW-1 CLOSED -->

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xl-16">
    <div class="card overflow-hidden bg-white work-progress">
      <div class="card-header">
        <h3 class="card-title">Statistik Kasus Coronavirus di Indonesia</h3>
      </div>
      <div class="card-body">
        <div id="chart" style="height: 300px;">
        {!! $chart->container() !!}
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
        {!! $chart->script() !!}
        <div class="row mt-6">
          <div class="col text-center">
            <div class="card bg-danger img-card box-primary-shadow">
              <div class="card-body">
                <div class="d-flex">
                  <div class="text-white">
                    <p class="text-white mb-0">TOTAL POSITIF</p>
                    <h2 class="mb-0 number-font">{{ number_format($indonesia['update']['total']['jumlah_positif'],0,",",".") }}</h2>
                    <small>+ {{ number_format($indonesia['update']['penambahan']['jumlah_positif'],0,",",".") }}</small>
                    <p class="text-white mb-0">ORANG</p>
                  </div>
                  <div class="ml-auto"> <img src="../uploads/sad-u6e.png" width="50" height="50" alt="Positif"> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col text-center">
            <div class="card bg-success img-card box-secondary-shadow">
              <div class="card-body">
                <div class="d-flex">
                  <div class="text-white">
                    <p class="text-white mb-0">TOTAL SEMBUH</p>
                    <h2 class="mb-0 number-font">{{ number_format($indonesia['update']['total']['jumlah_sembuh'],0,",",".") }}</h2>
                    <small>+ {{ number_format($indonesia['update']['penambahan']['jumlah_sembuh'],0,",",".") }}</small>
                    <p class="text-white mb-0">ORANG</p>
                  </div>
                  <div class="ml-auto"> <img src="../uploads/happy-ipM.png" width="50" height="50" alt="Positif"> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col text-center">
            <div class="card bg-dark img-card box-primary-shadow">
              <div class="card-body">
                <div class="d-flex">
                  <div class="text-white">
                    <p class="text-white mb-0">TOTAL MENINGGAL</p>
                    <h2 class="mb-0 number-font">{{ number_format($indonesia['update']['total']['jumlah_meninggal'],0,",",".") }}</h2>
                    <small>+ {{ number_format($indonesia['update']['penambahan']['jumlah_meninggal'],0,",",".") }}</small>
                    <p class="text-white mb-0">ORANG</p>
                  </div>
                  <div class="ml-auto"> <img src="../uploads/emoji-LWx.png" width="50" height="50" alt="Positif"> </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col text-center">
            <div class="card bg-orange img-card box-primary-shadow">
              <div class="card-body">
                <div class="d-flex">
                  <div class="text-white">
                    <p class="text-white mb-0">TOTAL DIRAWAT</p>
                    <h2 class="mb-0 number-font">{{ number_format($indonesia['update']['total']['jumlah_dirawat'],0,",",".") }}</h2>
                    <small>+ {{ number_format($indonesia['update']['penambahan']['jumlah_dirawat'],0,",",".") }}</small>
                    <p class="text-white mb-0">ORANG</p>
                  </div>
                  <div class="ml-auto"> <img src="../uploads/dirawat.png" width="50" height="50" alt="Positif"> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- COL END -->

</div>
<!-- ROW-2 CLOSED -->

<div class="row row-cards">
  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-14">
    <div class="card">
      <div class="card-header ">
        <h3 class="card-title">Data Kasus Coronavirus Global</h3>
      </div>
      <div class="card-body">
        <div class="table-responsive service">
          <table class="table table-bordered table-hover mb-0 text-nowrap css-serial" id="global">
            <thead>
              <tr>
                <th class="atasbro">NO.</th>
                <th class="atasbro">Negara</th>
                <th class="atasbro">Positif</th>
                <th class="atasbro">Sembuh</th>
                <th class="atasbro">Meninggal</th>
                <th class="atasbro">Dirawat</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($global['Countries'] as $key => $hasil)
                <tr>
                  <td>&nbsp;</td>
                  <td>{{$hasil['Country'] }}</td>
                  <td>{{number_format($hasil['TotalConfirmed'],0,",",".") }} <small style="color: red"> +{{number_format($hasil['NewConfirmed'],0,",",".") }}</small></td>
                  <td>{{number_format($hasil['TotalRecovered'],0,",",".") }} <small style="color: red"> +{{number_format($hasil['NewRecovered'],0,",",".") }}</small></td>
                  <td>{{number_format($hasil['TotalDeaths'],0,",",".")  }} <small style="color: red"> +{{number_format($hasil['NewDeaths'],0,",",".") }}</small></td>
                  <td>{{number_format($hasil['TotalConfirmed']-$hasil['TotalRecovered']-$hasil['TotalDeaths'],0,",",".")  }}<small style="color: red"> +{{number_format($hasil['NewConfirmed']-$hasil['NewRecovered']-$hasil['NewDeaths'],0,",",".") }}</small></td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row row-cards">
  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-14">
    <div class="card">
      <div class="card-header ">
        <h3 class="card-title">Data Kasus Coronavirus di Indonesia Berdasarkan Provinsi</h3>
      </div>
      <div class="card-body">
        <div class="table-responsive service">
          <table class="table table-bordered table-hover mb-0 text-nowrap css-serial" id="global">
            <thead>
              <tr>
                <th class="atasbro">NO.</th>
                <th class="atasbro">Provinsi</th>
                <th class="atasbro">Positif</th>
                <th class="atasbro">Sembuh</th>
                <th class="atasbro">Meninggal</th>
                <th class="atasbro">Dirawat</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($allprov['list_data'] as $result => $hasil)
                <tr>
                  <td>&nbsp;</td>
                  <td>{{$hasil['key'] }}</td>
                  <td>{{number_format($hasil['jumlah_kasus'],0,",",".") }} <small style="color: red"> +{{number_format($hasil['penambahan']['positif'],0,",",".") }}</small></td>
                  <td>{{number_format($hasil['jumlah_sembuh'],0,",",".") }} <small style="color: red"> +{{number_format($hasil['penambahan']['sembuh'],0,",",".") }}</small></td>
                  <td>{{number_format($hasil['jumlah_meninggal'],0,",",".")  }} <small style="color: red"> +{{number_format($hasil['penambahan']['meninggal'],0,",",".") }}</small></td>
                  <td>{{number_format($hasil['jumlah_dirawat'],0,",",".")  }} <small style="color: red"> +{{number_format($hasil['penambahan']['positif']-$hasil['penambahan']['sembuh']-$hasil['penambahan']['meninggal'],0,",",".") }}</td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
