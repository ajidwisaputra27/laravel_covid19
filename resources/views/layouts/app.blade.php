<!DOCTYPE html>
<html lang="en">

<head>
  <title>Info Corona by Aji Dev - Coronavirus Global & Indonesia Live Data</title>
	<link rel="shortcut icon" type="image/x-icon" href="/uploads/favicon.ico">
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">

  @include('layouts.module.head')
</head>

<body>
  <div class="page">
    <div class="page-main">
      {{-- @include('layouts.module.header') --}}

      <!-- Main Content -->
      <div class="container app-content">
          @yield('content')
      </div>
      @include('layouts.module.footer')
    </div>
  </div>

  @include('layouts.module.script')
</body>

</html>
