<!-- BOOTSTRAP JS -->
<script src="{{ asset('data/plugins/bootstrap/js/bootstrap.bundle.min.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>
<script src="{{ asset('data/plugins/bootstrap/js/popper.min.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>

<!-- SPARKLINE JS-->
<script src="{{ asset('data/js/jquery.sparkline.min.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>

<!-- CHART-CIRCLE JS-->
<script src="{{ asset('data/js/circle-progress.min.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>

<!-- RATING STAR JS -->
<script src="{{ asset('data/plugins/rating/jquery.rating-stars.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>

<!-- CHARTJS CHART JS-->
<script src="{{ asset('data/plugins/chart/Chart.bundle.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>
<script src="{{ asset('data/plugins/chart/utils.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>

<!-- C3.JS CHART JS -->
<script src="{{ asset('data/plugins/charts-c3/d3.v5.min.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>
<script src="{{ asset('data/plugins/charts-c3/c3-chart.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>

<!-- INPUT MASK JS -->
<script src="{{ asset('data/plugins/input-mask/jquery.mask.min.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>

<!-- CUSTOM SCROLLBAR JS -->
<script src="{{ asset('data/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>

<!-- SIDE-MENU JS -->
<script src="{{ asset('data/plugins/horizontal-menu/horizontal-menu.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>

<!-- PIETY CHART JS-->
<script src="{{ asset('data/plugins/peitychart/jquery.peity.min.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>
<script src="{{ asset('data/plugins/peitychart/peitychart.init.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>

<!--MORRIS CHARTS JS -->
<script src="{{ asset('data/plugins/morris/raphael-min.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>
<script src="{{ asset('data/plugins/morris/morris.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>

<!-- SIDEBAR JS -->
<script src="{{ asset('data/plugins/sidebar/sidebar.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>

<!-- INDEX JS -->
<script src="{{ asset('data/js/index54.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>

<!-- STICKY JS -->
<script src="{{ asset('data/js/stiky.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>

<!--CUSTOM JS -->
<script src="{{ asset('data/js/custom.js')}}" type="916743ee205ea1b637ad2622-text/javascript"></script>

<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="916743ee205ea1b637ad2622-|49" defer=""></script>
@yield('js')
