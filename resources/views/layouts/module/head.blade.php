<!-- BOOTSTRAP CSS -->
<link href="{{ asset('data/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />

<!-- STYLE CSS -->
<link href="{{ asset('data/css/newstyle.css')}}" rel="stylesheet"/>
<link href="{{ asset('data/css/skin-modes.css')}}" rel="stylesheet"/>

<!--HORIZONTAL CSS-->
<link href="{{ asset('data/plugins/horizontal-menu/horizontal-menu.css')}}" rel="stylesheet" />

<!--C3.JS CHARTS CSS -->
<link href="{{ asset('data/plugins/charts-c3/c3-chart.css')}}" rel="stylesheet"/>

<!--MORRIS CSS -->
<link href="{{ asset('data/plugins/morris/morris.css')}}" rel="stylesheet"/>

<!-- CUSTOM SCROLL BAR CSS-->
<link href="{{ asset('data/plugins/scroll-bar/jquery.mCustomScrollbar.css')}}" rel="stylesheet"/>

<!--- FONT-ICONS CSS -->
<link href="{{ asset('data/css/icons.css')}}" rel="stylesheet"/>

<!-- SIDEBAR CSS -->
<link href="{{ asset('data/plugins/sidebar/sidebar.css')}}" rel="stylesheet">

<!-- COLOR SKIN CSS -->
<link id="theme" rel="stylesheet" type="text/css" media="all" href="{{ asset('data/colors/color1.css')}}" />
