<!-- HEADER -->
<div class="header hor-header">
  <div class="container">
    <div class="d-flex">
      <a class="animated-arrow hor-toggle horizontal-navtoggle"><span></span></a>
      <div class="">
        <a class="header-brand1" href="https://kawalcorona.com/">
          <img src="../uploads/logo-ehi.png" class="header-brand-img desktop-logo" alt="logo">
          <img src="../uploads/logo-ehi.png" class="header-brand-img light-logo" alt="logo">
        </a><!-- LOGO -->
      </div>
      <div class="d-flex  ml-auto header-right-icons header-search-icon">

        <nav class="horizontalMenu clearfix">
        <ul class="horizontalMenu-list">
          <li aria-haspopup="true"><a href="/" ><i class="fe fe-home"></i><b>DASHBOARD</b></a></li>
          <li aria-haspopup="true"><a href="https://sekitarkita.id"><i class="fa fa-android"></i><b>SEKITARKITA.ID</b></a></li>
          <li aria-haspopup="true"><a href="/hotline"><i class="fe fe-phone"></i><b>HOTLINE</b></a></li>
          <li aria-haspopup="true"><a href="/api"><i class="fe fe-terminal"></i><b>API FOR DEVELOPERS</b></a></li>
          <li aria-haspopup="true"><a href="https://hack.co.id/kontak" target="_blank" class=""><i class="fe fe-mail"></i><b>KONTAK</b></a></li>
        </ul>
      </nav>

        <div class="dropdown d-md-flex">
          <a class="nav-link icon full-screen-link nav-link-bg">
            <i class="fe fe-maximize fullscreen-button"></i>
          </a>
        </div><!-- FULL-SCREEN -->

      </div>
    </div>
  </div>
</div>
<!-- End HEADER -->


<!-- Mobile Header -->
<div class="mobile-header hor-mobile-header">
  <div class="container">
    <div class="d-flex">
      <a class="animated-arrow hor-toggle horizontal-navtoggle"><span></span></a>
      <a class="header-brand" href="https://kawalcorona.com/">
        <img src="../uploads/logo-ehi.png" class="header-brand-img desktop-logo" alt="logo">
        <img src="../uploads/logo-ehi.png" class="header-brand-img desktop-logo mobile-light" alt="logo">
      </a>
      <div class="d-flex order-lg-2 ml-auto header-right-icons">


        <div class="dropdown d-md-flex">
        <nav class="horizontalMenu clearfix">
        <ul class="horizontalMenu-list">
          <li aria-haspopup="true"><a href="/" ><i class="fe fe-home"></i><b>DASHBOARD</b></a></li>
          <li aria-haspopup="true"><a href="https://sekitarkita.id"><i class="fe fe-download"></i><b>SEKITARKITA.ID</b></a></li>
          <li aria-haspopup="true"><a href="/hotline"><i class="fe fe-phone"></i><b>HOTLINE</b></a></li>
          <li aria-haspopup="true"><a href="/api"><i class="fe fe-terminal"></i><b>API FOR DEVELOPERS</b></a></li>
          <li aria-haspopup="true"><a href="https://hack.co.id/kontak" target="_blank" class=""><i class="fe fe-mail"></i><b>KONTAK</b></a></li>
        </ul>
      </nav>
        <a class="nav-link icon full-screen-link nav-link-bg">
          <i class="fe fe-maximize fullscreen-button"></i>
        </a>
      </div>
      </div>
    </div>
  </div>
</div>

<!-- /Mobile Header -->
