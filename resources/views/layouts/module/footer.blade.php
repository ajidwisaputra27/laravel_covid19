<!-- FOOTER -->
<br><br>
<footer>
<div class="footer border-top-0 footer-1">
  <div class="container">
    <div class="row align-items-center">
      {{-- <div class="social">
        <ul class="text-center">
          <li>
            <a class="social-icon" href="https://fb.me/ethicalhack.id" target="_blank"><i class="fa fa-facebook"></i></a>
          </li>
          <li>
            <a class="social-icon" href="https://instagram.com/ethicalhack.id" target="_blank"><i class="fa fa-instagram"></i></a>
          </li>
        </ul>
      </div> --}}
      <div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
        {{-- <br> --}}
        {{-- <a href="https://hack.co.id/"><img src="../uploads/logo-ehi.png" alt="Ethical Hacker Indonesia" width="172" height="45"></a> --}}
        {{-- <br><br> --}}
        Powered by <a href="https://kawalcorona.com" target="_blank">Kawal Corona</a>. Made with <i class="fa fa-heart"></i> by <a href="https://ajidwisaputra27.github.io/" target="_blank">Aji Dwi Saputra</a>
      </div>
    </div>
  </div>
</div>
</footer>
<!-- FOOTER END -->
