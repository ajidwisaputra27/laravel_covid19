<aside id="sidebar-wrapper">
  <div class="sidebar-brand">
    <a href="{{route('home')}}">PT. Java Sintesa Indotama</a>
  </div>
  <div class="sidebar-brand sidebar-brand-sm">
    <a href="{{route('home')}}">JSI</a>
  </div>
  <ul class="sidebar-menu">
    <li class="menu-header">Dashboard</li>
    <li class="@if(Request::segment(2) == 'home') active @endif">
      <a href="{{route('home')}}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
    </li>
    <li class="menu-header">Manajemen Produk</li>
    <li class="@if(Request::segment(2) == 'category') active @endif"><a class="nav-link"
        href="{{route('category.index')}}"><i class="far fa-clipboard"></i><span>Category</span></a>
    </li>
    <li class="@if(Request::segment(2) == 'product') active @endif"><a class="nav-link"
        href="{{route('product.index')}}"><i class="fas fa-book-open"></i><span>Product</span></a>
    </li>
    <li class="menu-header">Pengaturan</li>
    <li class="@if(Request::segment(2) == 'cp') active @endif"><a class="nav-link"
        href="{{route('cp.edit',['cp' => 1])}}"><i class="fas fa-cog"></i></i><span>Manajemen
          Perusahaan</span></a>
    </li>
    <li class="@if(Request::segment(2) == 'inbox') active @endif"><a class="nav-link" href="{{route('inbox.index')}}"><i
          class="fas fa-inbox"></i><span>Inbox</span></a>
    </li>
    <li class="@if(Request::segment(2) == 'user') active @endif"><a class="nav-link" href="#"><i
          class="far fa-user"></i><span>User</span></a>
    </li>
  </ul>

</aside>
